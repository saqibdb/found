//
//  AppDelegate.m
//  Glasses
//
//  Created by Hamza Temp on 12/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "AppDelegate.h"
#import "Found-Swift.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "Found-Swift.h"
#import <LGBluetooth.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@import GoogleMaps;

@interface AppDelegate (){
    NSTimer *bgTimer;
    NSMutableArray *allFoundPerphs;
    UIBackgroundTaskIdentifier counterTask;
    
    BOOL isBackground;
    
    NSMutableArray *myGlassesArray;
}

@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *currentLevelKey = @"firstTimeFoundCheck";
    if ([preferences objectForKey:currentLevelKey] == nil){
        //  Doesn't exist.
        [preferences setObject:@"AlreadyStarted" forKey:currentLevelKey];
        [preferences setBool:YES forKey:@"isBluetoothDisabledThenWantNotification"];
        [preferences setBool:YES forKey:@"isLeftBehind"];
    }
    [preferences synchronize];
    
    self.manager =  [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    self.manager.delegate = self;
    
    //    [GMSServices provideAPIKey:@"AIzaSyBhFVlLRae8GxDTT3gEWPKSaBM3uUUjZ-M"];
    [GMSServices provideAPIKey:@"AIzaSyBbuw7CWM04-A9QaQSVpA5IeuHfoF1gylk"];
    
    //[GMSPlacesClient provideAPIKey:@"AIzaSyBhFVlLRae8GxDTT3gEWPKSaBM3uUUjZ-M"];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
    
    [self requestNotificationAccess];
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"FAILED HERE");
    NSLog(@"didFailWithError: %@", error);
    
    if ([error.domain isEqualToString:kCLErrorDomain]) {
        return;
    }
    if (error.localizedDescription.length == 0) {
        return;
    }
    
    [Utils showLocalNotoficationWithTitle:@"Error" body:error.localizedDescription];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Name" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [application openURL:URL options:@{} completionHandler:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [alert addAction:ok];
    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    \
    CLLocation *currentLocation = newLocation;
    // [Utils showLocalNotoficationWithTitle:@"sample" body:@"notification"];
    if (currentLocation != nil) {
        NSString *lat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSString *lon = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSDictionary* userInfo = @{@"latitude": lat,@"longitude": lon};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.location.event" object:nil userInfo:userInfo];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    counterTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"Expired AGAIN");
        
        if (counterTask != UIBackgroundTaskInvalid) {
            [[UIApplication sharedApplication] endBackgroundTask:counterTask];
            counterTask = UIBackgroundTaskInvalid;
        }
        
    }];
    myGlassesArray = [GlassesPeripheral getAllGlasses];
    [self scanWithTime:2];
    
    isBackground = YES;
}

-(void)targetMethod:(NSTimer*)timer{
    NSLog(@"Tick...");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"applicationWillEnterForeground");
    isBackground = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    isBackground = NO;
    if (bgTimer) {
        [bgTimer invalidate];
        bgTimer = nil;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.background.event" object:nil ];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DatabaseModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DatabaseModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)scanWithTime :(int)time {
    if (isBackground == NO) {
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        
        if (![[LGCentralManager sharedInstance] isCentralReady]) {
            NSLog(@"Reason is %@" ,[[LGCentralManager sharedInstance] centralNotReadyReason] );
            return;
        }
        
        // Scaning 4 seconds for peripherals
        [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:time
                                                             completion:^(NSArray *peripherals)
         {
            // If we found any peripherals sending to test
            if (peripherals.count) {
                
                NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                
                NSString *currentLevelKey = @"FoundProximity";
                
                if ([preferences objectForKey:currentLevelKey] == nil){
                    //  Doesn't exist.
                    allFoundPerphs = [[NSMutableArray alloc] initWithArray:peripherals];
                }
                else{
                    //  Get current level
                    allFoundPerphs = [[NSMutableArray alloc] init];
                    
                    const NSInteger currentLevel = [preferences integerForKey:currentLevelKey];
                    
                    for (LGPeripheral *perph in peripherals) {//75
                        
                        int proxityNow = (int)floor([self estimatedDistanceWithRSSI:@(perph.RSSI) calibrationPower:@(-75)]);
                        
                        NSLog(@"RSSI  = %ld name = %@ Distance = %i" , (long)perph.RSSI , [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"], proxityNow);
                        
                        if (currentLevel > proxityNow) {
                            [allFoundPerphs addObject:perph];
                        }
                        else{
                            NSLog(@"Out of proximity");
                        }
                    }
                    
                }
                [self checkOfflineGlasses];
                [self scanWithTime: 2];
                
            }
            else{
                NSLog(@"NONE");
                allFoundPerphs = [[NSMutableArray alloc] init];
                
                [self checkOfflineGlasses];
                [self scanWithTime: 2];
            }
        }];
        
    });
    
}

-(void)checkOfflineGlasses {
    for (GlassesPeripheral *glasees in myGlassesArray) {
        
        BOOL foundInCurrentDevices = NO;
        
        for (LGPeripheral *perph in allFoundPerphs) {
            if ([perph.UUIDString isEqualToString:[glasees valueForKey:@"identifier"]]) {
                foundInCurrentDevices = YES;
                break;
            }
        }
        
        if (foundInCurrentDevices == YES) {
            [GlassesPeripheral editLastStatus:glasees newValue:@"online"];
        }
        else{
            NSString *lastOnlineStr = [glasees valueForKey:@"lastStatus"];
            if ([lastOnlineStr isEqualToString:@"online"]) {
                
                NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                
                BOOL isLeftBehind = [preferences boolForKey:@"isLeftBehind"];
                if (isLeftBehind == YES){
                    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                    
                    NSString *currentLevelKey = @"notificationDisableTime";
                    
                    if ([preferences objectForKey:currentLevelKey] == nil){
                        //  Doesn't exist.
                    }
                    else{
                        
                        int notificationDisableTime = [[preferences objectForKey:currentLevelKey] intValue];
                        
                        UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
                        content.title = [NSString localizedUserNotificationStringForKey:@"Out of Proximity" arguments:nil];
                        
                        NSString *msg = [NSString stringWithFormat:@"You left your glasses behind!"];
                        
                        if ([glasees valueForKey:@"name"] != nil) {
                            msg = [NSString stringWithFormat:@"You left your %@ glasses behind!", [glasees valueForKey:@"name"]];
                        }
                        
                        content.body = [NSString localizedUserNotificationStringForKey:msg arguments:nil];
                        
                        UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:(notificationDisableTime * 60) repeats:NO];
                        UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"notificationOut" content:content trigger:trigger];
                        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                            if (error) {
                                NSLog(@"ERROR on Notification = %@" , error.description);
                            }
                        }];
                        
                        break;
                    }
                }
                else{
                }
            }
            
            [GlassesPeripheral editLastStatus:glasees newValue:@"offline"];
            
        }
    }
}

-(float)estimatedDistanceWithRSSI:(NSNumber *)RSSI calibrationPower:(NSNumber *)calibrationPower {
    if (calibrationPower == 0) {
        return -1.0; // if we cannot determine distance, return -1.
    }
    
    float rssi = [RSSI floatValue];
    if (rssi == 0) {
        return -1.0; // if we cannot determine distance, return -1.
    }
    
    double ratio = rssi*1.0/[calibrationPower floatValue];
    if (ratio < 1.0) {
        return (pow(ratio,10) * 3.28084);
    } else {
        double accuracy =  (0.89976)*pow(ratio,7.7095) + 0.111;
        return (accuracy * 3.28084);
    }
}

@end
