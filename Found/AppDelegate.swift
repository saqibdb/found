//
//  AppDelegate.swift
//  Glasses
//
//  Created by Jefri Singh on 11/03/18.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import CoreBluetooth
import Fabric
import Crashlytics

@objc extension AppDelegate:UNUserNotificationCenterDelegate,CBCentralManagerDelegate{
    
    // Delegate method
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        Utils.showLocalNotofication(title: "Bluetooth Disconnected", body: "Warning: Bluetooth need to be enable!")
    }
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        //Utils.showLocalNotofication(title: "Bluetooth Connected!", body: "Warning: Bluetooth need to be enable!")
    }
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if central.state == .poweredOn {
            
        }
        else if central.state == .resetting{
            
        }
        else if central.state == .unauthorized
        {
            
        }
        else if central.state == .unknown
        {
            
        }
        else if central.state == .unsupported
        {
            
        }
        else if central.state == .poweredOff{
            
            Utils.showLocalNotofication(title: "Bluetooth Disconnected", body: "Warning: Bluetooth need to be enable!")
        }
    }
    
    func requestNotificationAccess(){
        
        func setCategories(){
            let snoozeAction = UNNotificationAction(identifier: "workout",title: "1MB",options: [])
            let alarmCategory = UNNotificationCategory(
                identifier: "alarm.category",
                actions: [snoozeAction],
                intentIdentifiers: [],
                options: [])
            UNUserNotificationCenter.current().setNotificationCategories([alarmCategory])
        }
        
        
        UNUserNotificationCenter.current().requestAuthorization( options: [.alert, .sound, .badge], completionHandler: {(_ granted, _ error) in
            if granted == true {
                
                setCategories()
            }
        })
        
        UNUserNotificationCenter.current().delegate = self
    }
    
    func addNotification(content:UNNotificationContent,trigger:UNNotificationTrigger?, indentifier:String){
        let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: {
            (errorObject) in

            if let error = errorObject{
                print("Error \(error.localizedDescription) in notification \(indentifier)")
            }
        })
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let content = response.notification.request.content
        
        if content.body.contains("http"){
            
//            self.videoURL = content.body
//            self.videoTittle = response.notification.request.content.title
//            self.openMainViewController()
            
        }else{
//            self.videoURL = ""
        }
    }
    
}
