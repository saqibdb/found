//
//  DisableNotificationCell.h
//  Glasses
//
//  Created by Hamza Temp on 17/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisableNotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
