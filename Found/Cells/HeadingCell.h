//
//  HeadingCell.h
//  Glasses
//
//  Created by Hamza Temp on 17/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
