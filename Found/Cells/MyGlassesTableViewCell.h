//
//  MyGlassesTableViewCell.h
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGlassesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *glassesImage;
@property (weak, nonatomic) IBOutlet UIImageView *statusImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIButton *goingNextBtn;

@property (weak, nonatomic) IBOutlet UIButton *ellipsisBtn;



@end
