//
//  NewGlassesTableViewCell.h
//  Found
//
//  Created by MacBook Pro on 20/01/2020.
//  Copyright © 2020 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewGlassesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameTitle;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;



@end

NS_ASSUME_NONNULL_END
