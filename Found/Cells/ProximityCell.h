//
//  MyGlassesTableViewCell.h
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProximityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewSelected;
@property (weak, nonatomic) IBOutlet UILabel *labelProximityTitle;
@end
