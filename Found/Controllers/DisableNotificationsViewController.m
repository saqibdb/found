//
//  DisableNotificationsViewController.m
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "DisableNotificationsViewController.h"

@interface DisableNotificationsViewController ()

@end

@implementation DisableNotificationsViewController
@synthesize checkMark;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonPressed:(id)sender {
    
    UIButton *button = (UIButton*)sender;
    
    float buttonCenter = button.frame.origin.y + button.frame.size.height/2;
    
    
    
    CGRect rect = CGRectMake(self.checkMark.frame.origin.x, buttonCenter - self.checkMark.frame.size.height/2, self.checkMark.frame.size.width, self.checkMark.frame.size.height);
    
     //CGRect rect2 = CGRectMake(self.checkMark.frame.origin.x, self.checkMark.frame.origin.y - , self.checkMark.frame.size.width, self.checkMark.frame.size.height);
    
    
    
    
    self.checkMark.frame = rect;
    
     
}
- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
