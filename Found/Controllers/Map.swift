//
//  ViewController.swift
//  GrayMapIntegrationTest
//
//  Created by Sreelash on 03/11/17.
//  Copyright © 2017 exalt security solutions. All rights reserved.
//

import UIKit
import GoogleMaps

@objc
public class Map: UIViewController {
    
    @IBOutlet var mapView:GMSMapView!
    
    @objc
    var selectedGlasses:GlassesPeripheral?
    
    var location = [(latitude:"56.655789",longitude:"76.8504593"),(latitude:"8.565128",longitude:"76.8504593")]
    
    var polyline:GMSPolyline!
    @IBOutlet var labelGlassName: UILabel!
    @IBOutlet var labelLastSeen: UILabel!
    
    private var isRouteEnabled:Bool = false
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        if let lat = selectedGlasses?.latitude{
            
            self.location[1].latitude = lat
        }
        
        if let lan = selectedGlasses?.longitude{
            
            self.location[1].longitude = lan
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: Double(selectedGlasses!.latitude)!, longitude:  Double(selectedGlasses!.latitude)!, zoom: 6.0)
        self.mapView.isMyLocationEnabled = true
        self.mapView.camera = camera

        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(selectedGlasses!.latitude)!, longitude:  Double(selectedGlasses!.latitude)!)
        marker.title = selectedGlasses?.name ?? ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy hh:mm at"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        
        marker.snippet = "Found at "+formatter.string(from: selectedGlasses!.dateInserted)
        
        self.labelLastSeen.text = "Found at "+formatter.string(from: selectedGlasses!.dateInserted)
        self.labelGlassName.text = self.selectedGlasses?.name
        
        marker.map = mapView
        marker.icon = UIImage(named: "Glass Pin")
    
  
        NotificationCenter.default.addObserver(self, selector: #selector(self.trigerAction(_:)), name: NSNotification.Name(rawValue: "com.location.event"), object: nil)
        
    }
    

    @objc func trigerAction(_ notification:Notification){
        
        let dictionary = notification.userInfo
        
        if let latitude = dictionary!["latitude"] as? String{
            
            self.location[0].latitude = latitude
        }
        if let longitude = dictionary!["longitude"] as? String{
            
            self.location[0].longitude = longitude
        }
        
        if isRouteEnabled{
            self.drawRout()
        }
    }
   
    
    @IBAction func actionDirection(_ sender: UIButton) {
        self.isRouteEnabled = true
        self.drawRout()
    }
    
    @IBAction func actionBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func drawRout(){
        
        print("Ready to Draw Riyt")
        let origin = "\(self.location[0].latitude),\(self.location[0].longitude)"
        let destination = "\(self.location[1].latitude),\(self.location[1].longitude)"
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBUhBi6Hauc_7k-omB6uxBz9kDETT03MdM&sensor=true"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                    print("JSON:",json)
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            self.polyline = GMSPolyline.init(path: path)
                            self.polyline.strokeWidth = 3
                            self.polyline.strokeColor = UIColor.black
                            
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                     
                            self.polyline.map = self.mapView
                            
                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


