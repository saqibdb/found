//
//  PairingViewController.h
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBPulseButton.h"

@interface PairingViewController : UIViewController <UITabBarDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDataSource , UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *connectedView;
@property (weak, nonatomic) IBOutlet UIView *connectingView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBtn;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *outerCircle;
@property (weak, nonatomic) IBOutlet UIImageView *midCircle;

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@property (weak) NSTimer* timer;

@property (weak, nonatomic) IBOutlet UILabel *connectedText;
@property (weak, nonatomic) IBOutlet UILabel *glassesNameText;
@property (weak, nonatomic) IBOutlet UILabel *serialNumberText;
@property (weak, nonatomic) IBOutlet UIImageView *glassesLogo;


@property (weak, nonatomic) IBOutlet YBPulseButton *pulseBtn;


@property (weak, nonatomic) IBOutlet UITableView *nGlassesTableView;



@end
