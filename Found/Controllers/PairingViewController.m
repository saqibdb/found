//
//  PairingViewController.m
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "PairingViewController.h"
#import "ViewController.h"
#import <LGBluetooth.h>
#import "GlassesPeripheral.h"
#import "Found-Swift.h"
#import "NewGlassesTableViewCell.h"


@interface PairingViewController (){
    NSMutableArray *myGlassesArray;
    GlassesPeripheral *newGlasses;
    NSMutableArray *allFoundPerphs;
    
    BOOL isConnected;
}

@end


@implementation PairingViewController

@synthesize midCircle,outerCircle,connectedView,connectingView,closeBtn,doneBtn,timer,tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    [LGCentralManager sharedInstance];
    
    // Do any additional setup after loading the view.
    [self.connectedView setHidden:true];
    [self.connectingView setHidden:false];
    [self.doneBtn setEnabled:false];
    
    [self.midCircle setHidden:true];
    [self.outerCircle setHidden:true];
    
    self.tabBar.delegate = self;
    
    int count = (int)[self.tabBar.items count];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:@"com.background.event" object:nil ];
    
    for (int i = 0 ; i < count ; i ++)
    {
        UITabBarItem * item =  [self.tabBar.items objectAtIndex:i];
        [item.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    
    
    myGlassesArray = [GlassesPeripheral getAllGlasses];
    
    [self.view layoutIfNeeded];
    self.pulseBtn.cornerRadius = self.pulseBtn.frame.size.width / 2;
    [self.view layoutIfNeeded];
    
    
    self.nGlassesTableView.tag = 2;
    self.nGlassesTableView.delegate = self;
    self.nGlassesTableView.dataSource = self;
    self.nGlassesTableView.hidden = YES;
    isConnected = NO;
    
    
        newGlasses = [[GlassesPeripheral alloc] init];
        newGlasses.name = @"Device 1";
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    
        newGlasses.identifier = [dateFormatter stringFromDate:[NSDate date]];
    
        newGlasses.dateInserted = [NSDate date];
        UIImage *image = [UIImage imageNamed:@"GlassesPhoto"];
        newGlasses.image = [self encodeToBase64String:image];
        newGlasses.proximity = @"10";
        newGlasses.latitude = @"10.8713484";
        newGlasses.longitude = @"77.5345086";
    [self.doneBtn setEnabled:YES];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self animateForConnection];
    [self scanWithTime:1];
    
    //    //Duplicate
    ////
    //    newGlasses = [[GlassesPeripheral alloc] init];
    //    newGlasses.name = @"Device 1";
    //    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    //    [dateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    //
    //    newGlasses.identifier = [dateFormatter stringFromDate:[NSDate date]];
    //
    //    newGlasses.dateInserted = [NSDate date];
    //    UIImage *image = [UIImage imageNamed:@"GlassesPhoto"];
    //    newGlasses.image = [self encodeToBase64String:image];
    //    newGlasses.proximity = @"10";
    //    newGlasses.latitude = @"10.8713484";
    //    newGlasses.longitude = @"77.5345086";
    //
    
}



//event handler when event occurs
-(void)eventHandler: (NSNotification *) notification
{
    NSLog(@"event triggered");
    [self animateForConnection];
    [self scanWithTime:1];
}

-(BOOL) doesAlertViewExist {
    if ([[UIApplication sharedApplication].keyWindow isMemberOfClass:[UIWindow class]])
    {
        return NO;//AlertView does not exist on current window
    }
    return YES;//AlertView exist on current window
}

-(void)scanWithTime :(int)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pulseBtn ceateAGenericPulse];
        //Your main thread code goes in here
        
        if (self.isBluetoothOn == NO){
            if ([self doesAlertViewExist] == NO){
                
                [self checkBluetoothWithIsRepeat:NO];
                return;
            }
        }else{
            
            if([self doesAlertViewExist] == YES){
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
        if (![[LGCentralManager sharedInstance] isCentralReady]) {
            NSLog(@"Reason is %@" ,[[LGCentralManager sharedInstance] centralNotReadyReason] );
            return;
        }
        // Scaning 4 seconds for peripherals
        [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:time
                                                             completion:^(NSArray *peripherals)
         {
            self->allFoundPerphs = [[NSMutableArray alloc] init];
            // If we found any peripherals sending to test
            if (peripherals.count) {
                for (LGPeripheral *perph in peripherals) {
                    if (self->myGlassesArray.count) {
                        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.identifier LIKE[cd] %@",perph.UUIDString];
                        
                        NSArray *searchedArray = [self->myGlassesArray filteredArrayUsingPredicate:resultPredicate];
                        
                        if (!searchedArray.count) {
                            NSLog(@"BLE FOUND RSSI = %ld with name = %@" , (long)perph.RSSI , [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"]);
                            
                            [self->allFoundPerphs addObject:perph];
                            
                            
                            //[self putPeripheralOnFound:perph];
                            //[self showConnectedScreen];
                            //return;
                        }
                        
                    }
                    else{
                        [self->allFoundPerphs addObject:perph];
                        //[self putPeripheralOnFound:perph];
                        //[self showConnectedScreen];
                        //return;
                    }
                    
                }
                [self.nGlassesTableView reloadData];
                if (self->allFoundPerphs.count) {
                    if (self->isConnected == NO) {
                        self.nGlassesTableView.hidden = NO;
                    }
                }
                else{
                    self.nGlassesTableView.hidden = YES;
                    
                }
                
                [self scanWithTime: 1];
            }
            else{
                NSLog(@"NONE");
                [self scanWithTime: 1];
                self.nGlassesTableView.hidden = YES;
                
            }
        }];
        
    });
    
}


-(void)putPeripheralOnFound :(LGPeripheral *) perph{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *signalStr = @"WEAK";
        if (perph.RSSI > -85) {
            signalStr = @"STRONG";
        }
        
        
        self.glassesNameText.text = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
        self.serialNumberText.text = [NSString stringWithFormat:@"Signal Strength = %@", signalStr];
        
        self->newGlasses = [[GlassesPeripheral alloc] init];
        self->newGlasses.name = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
        self->newGlasses.identifier = perph.UUIDString;
        self->newGlasses.dateInserted = [NSDate date];
        UIImage *image = [UIImage imageNamed:@"GlassesPhoto"];
        self->newGlasses.image = [self encodeToBase64String:image];
    });
    
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}


- (void)testPeripheral:(LGPeripheral *)peripheral
{
    // First of all, opening connection
    [peripheral connectWithCompletion:^(NSError *error) {
        // Discovering services of peripheral
        [peripheral discoverServicesWithCompletion:^(NSArray *services, NSError *error) {
            // Searching in all services, our - 5ec0 service
            for (LGService *service in services) {
                if ([service.UUIDString isEqualToString:@"5ec0"]) {
                    // Discovering characteristics of 5ec0 service
                    [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                        __block int i = 0;
                        // Searching writable characteristic - cef9
                        for (LGCharacteristic *charact in characteristics) {
                            if ([charact.UUIDString isEqualToString:@"cef9"]) {
                                [charact writeByte:0xFF completion:^(NSError *error) {
                                    if (++i == 3) {
                                        [peripheral disconnectWithCompletion:nil];
                                    }
                                }];
                            } else {
                                // Otherwise reading value
                                [charact readValueWithBlock:^(NSData *data, NSError *error) {
                                    if (++i == 3) {
                                        [peripheral disconnectWithCompletion:nil];
                                    }
                                }];
                            }
                        }
                    }];
                }
            }
        }];
    }];
}

#pragma mark - Animation

-(void)animateForConnection
{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showAnimation) userInfo:nil repeats:true];
}

-(void)showAnimation
{
    if(self.midCircle.isHidden && self.outerCircle.isHidden)
    {
        [self.midCircle setHidden:false];
    }
    else if ( self.midCircle.isHidden == false && self.outerCircle.isHidden)
    {
        [self.midCircle setHidden:false];
        [self.outerCircle setHidden:false];
    }
    else if(self.midCircle.isHidden == false && self.outerCircle.isHidden == false)
    {
        [self.midCircle setHidden:true];
        [self.outerCircle setHidden:true];
    }
}


-(void)showConnectedScreen
{
    [self.connectedView setHidden:false];
    [self.connectingView setHidden:true];
    [self.nGlassesTableView setHidden:true];
    
    [self.doneBtn setEnabled:true];
    isConnected = YES;
    // stop timer
    [timer invalidate];
    timer = nil;
}

#pragma mark - Button Methods
- (IBAction)closeBtnPressed:(id)sender {
    
    //    if (newGlasses) {
    //        NSError *error = [GlassesPeripheral createNewGlassesWithGlasses:newGlasses];
    //        if (error) {
    //            NSLog(@"Something is Wrong");
    //        }
    //    }
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    CATransition* transition = [CATransition animation];
    transition.duration = .5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    transition.subtype = kCATransitionFromBottom; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:@"someAnimation"];
    
    [self.navigationController popViewControllerAnimated:YES];
    [CATransaction commit];
    
    
    //[self.navigationController popViewControllerAnimated:true];
}
- (IBAction)doneBtnPressed:(id)sender {
    if (newGlasses) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: newGlasses.name message: @"Enter the name of the BLE device!" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"name";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.text = self->newGlasses.name;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            self->newGlasses.name = namefield.text;
            NSError *error = [GlassesPeripheral createNewGlassesWithGlasses:self->newGlasses];
            if (error) {
                NSLog(@"Something is Wrong");
            }
            
            [alertController dismissViewControllerAnimated:NO completion:^{
                
                
                UIAlertController* alertCtrl = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                //Create an action
                UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = self;
                    imagePicker.allowsEditing = true;
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:imagePicker animated:YES completion:nil];
                }];
                UIAlertAction *imageGallery = [UIAlertAction actionWithTitle:@"Image Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = self;
                    imagePicker.allowsEditing = true;
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:imagePicker animated:YES completion:nil];
                }];
              
                //Add action to alertCtrl
                [alertCtrl addAction:camera];
                [alertCtrl addAction:imageGallery];
                [self presentViewController:alertCtrl animated:YES completion:nil];

            
            }];
            
            
            //[self.navigationController popViewControllerAnimated:NO];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage * img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    UIImage * chosenImage = [self normalizedImageWithImage:img];
    NSLog(@"ORIENTATION = %i",[[[info objectForKey:@"UIImagePickerControllerMediaMetadata"] objectForKey:@"Orientation"] intValue]);

    
    NSString *encoded = [self encodeToBase64String:chosenImage];
    [GlassesPeripheral editImage:newGlasses newImage:encoded];
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popViewControllerAnimated:NO];
    }];
    
}


- (UIImage *)normalizedImageWithImage:(UIImage *)orgImage {
    if (orgImage.imageOrientation == UIImageOrientationUp) return orgImage;

    UIGraphicsBeginImageContextWithOptions(orgImage.size, NO, orgImage.scale);
    [orgImage drawInRect:(CGRect){0, 0, orgImage.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}







- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popViewControllerAnimated:NO];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allFoundPerphs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewGlassesTableViewCell *cell = (NewGlassesTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"NewGlassesTableViewCell" forIndexPath:indexPath];
    LGPeripheral *perph = allFoundPerphs[indexPath.row];
    if(perph.name != nil){
        cell.nameTitle.text = perph.name;
    }
    else if ([perph.advertisingData objectForKey:@"kCBAdvDataLocalName"] != nil){
        cell.nameTitle.text = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
    }
    else{
        cell.nameTitle.text = @"Unamed";
    }
    cell.subtitle.text = [NSString stringWithFormat:@"%lu services found" , (unsigned long)perph.services.count];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LGPeripheral *perph = allFoundPerphs[indexPath.row];
    [self putPeripheralOnFound:perph];
    [self showConnectedScreen];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */




@end
