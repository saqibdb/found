//
//  ProximitySetting.h
//  Glasses
//
//  Created by Jefri Singh on 10/03/18.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ProximitySetting : UITableViewController
@property (nonatomic) GlassesPeripheral *selectedGlasses;
@property (nonatomic) NSInteger selectedCell;
@end

