//
//  ProximitySetting.m
//  Glasses
//
//  Created by Jefri Singh on 10/03/18.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GlassesPeripheral.h"
#import "ProximitySetting.h"

@interface ProximitySetting ()


@end

@implementation ProximitySetting

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"cell"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
     if (cell == nil) {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"cell"];
     }
    // NSArray *myArray = @[@"10", @"20", @"30",@""];
     NSString *title = @"fas";
     cell.textLabel.text = [NSString stringWithFormat:@"%@ ft", title];
     return cell;
 }

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(self.selectedCell == indexPath.row){
//         [cell setSelected:YES animated:YES];
//    }else{
//   //     NSArray *myArray = @[@"10", @"20", @"30"];
//        NSString *title = @"ss";
//        if (self.selectedGlasses.proximity == title) {
//            [cell setSelected:YES animated:YES];
//        }else{
//           [cell setSelected:NO animated:YES];
//        }
//    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedCell = indexPath.row;

    [self.tableView reloadData];
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
