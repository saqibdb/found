//
//  ProximitySettingsViewController.swift
//  Glasses
//
//  Created by Jefri Singh on 11/03/18.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

import UIKit

@objc
public class ProximitySettingsViewController: UITableViewController {

    private var lastSelection: IndexPath?
    
    @objc
    var selectedGlasses:GlassesPeripheral?
    
    var proximities = [10,20,30]
    
    //Use below variable for checking
    
    @objc
    var isSettingsPage = false
    
    @objc
    var isProximity = false
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        if !self.isProximity{
            
            self.proximities = [2,4,6,7,12]
        }
 
    }


    // MARK: - Table view data source

    override public func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return self.proximities.count
    }


    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let proximity = self.proximities[indexPath.row]
        
        
        
        let preferences = UserDefaults.standard
        
        let currentLevelKey = "FoundProximity"
        if preferences.object(forKey: currentLevelKey) == nil {
            cell.accessoryType = .none
        } else {
            let currentLevel = preferences.integer(forKey: currentLevelKey)
            if proximity == currentLevel {
                cell.accessoryType = .checkmark
            }
            else{
                cell.accessoryType = .none
            }
        }
        
        
        /*
        if self.lastSelection == nil{
            if !self.isSettingsPage{
                if Int(self.selectedGlasses!.proximity)! == proximity{
                    cell.accessoryType = .checkmark
                    self.lastSelection = indexPath
                }else{
                     cell.accessoryType = .none
                }
            }else{
                
                if self.isProximity{
                    
                    if proximity == Utils.overallProximity(){
                        
                        cell.accessoryType = .checkmark
                        self.lastSelection = indexPath
                    }else{
                        cell.accessoryType = .none
                    }
                    
                }else{
                    
                    if proximity == Utils.notificationDisableTime(){
                        
                        cell.accessoryType = .checkmark
                        self.lastSelection = indexPath
                    }else{
                        cell.accessoryType = .none
                    }
                }
            }
        }*/
        
        cell.textLabel?.text = String(proximity)+(self.isProximity ? " ft" : " fts")
        
        return cell
    }
 

    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let preferences = UserDefaults.standard
        
        let currentLevel = self.proximities[indexPath.row]
        let currentLevelKey = "FoundProximity"
        preferences.set(currentLevel, forKey: currentLevelKey)
        preferences.synchronize()
        
        
        
        
        
        if self.isSettingsPage{
            
            if self.isProximity{
                 UserDefaults.standard.set(self.proximities[indexPath.row], forKey: "proximity")
            }else{
                UserDefaults.standard.set(self.proximities[indexPath.row], forKey: "notificationDisableTime")
            }
            
        }else{
            
            GlassesPeripheral.editProximity(self.selectedGlasses!, newValue: String(self.proximities[indexPath.row]))
        }
        
        if self.lastSelection != nil {
            
            self.tableView.cellForRow(at: self.lastSelection!)?.accessoryType = .none
        }
        
        self.tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        self.lastSelection = indexPath
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        self.tableView.reloadData()
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
