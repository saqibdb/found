//
//  SettingViewController.h
//  Glasses
//
//  Created by Hamza Temp on 16/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController <UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
