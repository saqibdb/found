//
//  SettingsViewController.m
//  Glasses
//
//  Created by Hamza Temp on 17/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "SettingsViewController.h"
#import "NotificationCell.h"
#import "DisableNotificationCell.h"
#import "AboutCell.h"
#import "HeadingCell.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize tabBar,tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tabBar.delegate = self;
    // Do any additional setup after loading the view.
    
     tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    int count = [self.tabBar.items count];
    
    
    for (int i = 0 ; i < count ; i ++)
    {
        UITabBarItem * item =  [self.tabBar.items objectAtIndex:i];
        [item.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell;
    if( indexPath.row == 0)
    {
    
   HeadingCell* hCell = (HeadingCell*)[tableView dequeueReusableCellWithIdentifier:@"hCell" forIndexPath:indexPath];
        
        hCell.label.text = @"NOTIFICATIONS";
        
        return hCell;
    }
    else if ( indexPath.row == 1)
    {
        
        NotificationCell* nCell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"nCell" forIndexPath:indexPath];
        
        nCell.label.text = @"When left behind";
        return nCell;
    }
    else if ( indexPath.row == 2)
    {
        
        NotificationCell* nCell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"nCell" forIndexPath:indexPath];
        
        nCell.label.text = @"When Bluetooth is disabled";
        return nCell;
    }
    else if ( indexPath.row == 3)
    {
        
        HeadingCell* hCell = (HeadingCell*)[tableView dequeueReusableCellWithIdentifier:@"hCell" forIndexPath:indexPath];
        
        hCell.label.text = @"DISABLE NOTIFICATIONS";
        return hCell;
    }
    else if ( indexPath.row == 4)
    {
        
        DisableNotificationCell* dCell = (DisableNotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"dCell" forIndexPath:indexPath];
        
        dCell.label.text = @"Duration";
        return dCell;
    }
    else if ( indexPath.row == 5)
    {
        
        HeadingCell* hCell = (HeadingCell*)[tableView dequeueReusableCellWithIdentifier:@"hCell" forIndexPath:indexPath];
        
        hCell.label.text = @"ABOUT";
        return hCell;
    }
    else if ( indexPath.row == 6)
    {
        
        AboutCell* aCell = (AboutCell*)[tableView dequeueReusableCellWithIdentifier:@"aCell" forIndexPath:indexPath];
        
        aCell.label.text = @"Follow us on Facebook";
        return aCell;
    }
    else if ( indexPath.row == 7)
    {
        
        AboutCell* aCell = (AboutCell*)[tableView dequeueReusableCellWithIdentifier:@"aCell" forIndexPath:indexPath];
        
        aCell.label.text = @"Legal and Privacy";
        return aCell;
    }
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedRow = indexPath.row; //this is the number row that was selected
    if(selectedRow == 4)
    {
        [self performSegueWithIdentifier:@"dNotify" sender:tableView];

    }
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    // Do Stuff!
    // if(item.title == @"First") {...}
    
    if(item.tag == 0)
    {
        [self popoverPresentationController];
        [self performSegueWithIdentifier:@"glassesList" sender:item];
        
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
