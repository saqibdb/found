//
//  MapViewController.h
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlassesPeripheral.h"

@import GoogleMaps;

@interface MapViewController : UIViewController <UIActionSheetDelegate, UITabBarDelegate, UITabBarDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *tabView;
@property (weak, nonatomic) IBOutlet UIView *proximatyView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;


@property (nonatomic) GlassesPeripheral *selectedGlasses;
@property (weak, nonatomic) IBOutlet UILabel *glassesNameText;
@property (weak, nonatomic) IBOutlet UILabel *glassesDate;


@end
