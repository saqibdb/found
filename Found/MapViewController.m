//
//  MapViewController.m
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Found-Swift.h"

@interface MapViewController ()
{
    CLLocationManager *locationManager;
    GMSMapView *map;
    NSString *latitude;
    NSString *longitude;
    GMSMarker *marker;
    BOOL isMarkerAdded;
    GMSPolyline *polyline;
    
}

@end

@implementation MapViewController
@synthesize mapView,tabView,tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.glassesNameText.text = self.selectedGlasses.name;

    

    map.myLocationEnabled = YES;
    //self.mapView = map;
    [self.mapView addSubview:map];
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.title = _selectedGlasses.name;
    marker.snippet = _selectedGlasses.identifier;
    marker.icon = [self decodeBase64ToImage:_selectedGlasses.image];
    
    
    
    marker.map = map;
    
    NSString *latstringDestination = [[NSString alloc] initWithString:_selectedGlasses.latitude] ;
    NSString *lonstringDestination = [[NSString alloc] initWithString:_selectedGlasses.longitude];
    
    double latdouble = [latstringDestination doubleValue];
    double londouble = [lonstringDestination doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latdouble longitude:londouble zoom:12];
    map = [GMSMapView mapWithFrame:mapView.frame camera:camera];
    marker.position = CLLocationCoordinate2DMake(latdouble, londouble);
    
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    //Configure Accuracy depending on your needs, default is kCLLocationAccuracyBest
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    
    [locationManager startUpdatingLocation];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(triggerAction:) name:@"com.location.event" object:nil];
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSDictionary *dict = notification.userInfo;
    latitude = [dict valueForKey:@"latitude"];
    longitude = [dict valueForKey:@"longitude"];
    
    [self drawRoute];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    latitude = @"10.4543296";
    longitude = @"78.1011628";
    
    [self drawRoute];
}


- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

- (void)drawRoute
{
    [self fetchPolylineWithOrigin:@"" completionHandler:^(GMSPolyline *polyline)
     {
         
         polyline.map = map;
    }];
}

- (void)fetchPolylineWithOrigin:(NSString *)origin completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *latstring = [[NSString alloc] initWithString:latitude] ;
    NSString *lonstring = [[NSString alloc] initWithString:longitude];
    
    double latdouble = [latstring doubleValue];
    double londouble = [lonstring doubleValue];
    
    NSString *latstringDestination = [[NSString alloc] initWithString:_selectedGlasses.latitude] ;
    NSString *lonstringDestination = [[NSString alloc] initWithString:_selectedGlasses.longitude];
    
    double latdoubleDestination = [latstringDestination doubleValue];
    double londoubleDestination = [lonstringDestination doubleValue];
    
    NSString *originString = [NSString stringWithFormat:@"%f,%f", latdouble, londouble];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", latdoubleDestination, londoubleDestination];
    
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving", directionsAPI, originString, destinationString];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     

                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                         completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                                     
                                                     polyline = nil;
                                                     if ([routesArray count] > 0)
                                                     {
                                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                         GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                         polyline = [GMSPolyline polylineWithPath:path];
                                                     }
                                             
                                                         if(completionHandler)
                                                         completionHandler(polyline);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

- (IBAction)proximityPressed:(id)sender {

    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Set Proximity" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"10 ft" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"20 ft" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"30 ft" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
   
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (IBAction)directionPressed:(id)sender {
   
    
}
- (IBAction)editBtnPressed:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Edit" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [GlassesPeripheral deleteGlasses:self.selectedGlasses];
        
        [self dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }]];
    
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    // Do Stuff!
    // if(item.title == @"First") {...}
    
    if(item.tag == 0)
    {
         [self popoverPresentationController];
        [self performSegueWithIdentifier:@"glassesList" sender:item];
        
    }
    else
    {
         [self popoverPresentationController];
        [self performSegueWithIdentifier:@"settings" sender:item];
    }
}
- (IBAction)closeBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}



@end
