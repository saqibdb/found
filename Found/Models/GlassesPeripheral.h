//
//  GlassesPeripheral.h
//  Glasses
//
//  Created by iBuildx-Macbook on 05/12/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface GlassesPeripheral : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *image;
@property (nonatomic) NSString *identifier;
@property (nonatomic) NSDate *dateInserted;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *proximity;
@property (nonatomic) NSString *lastStatus;

//

- (id)initWithManagedObject:(NSManagedObject *)object;
+ (NSError *)createNewGlassesWithGlasses :(GlassesPeripheral *)glasses;
+ (NSMutableArray *)getAllGlasses;
+ (void)deleteGlasses:(GlassesPeripheral *)glasses;
+ (void)editImage:(GlassesPeripheral *)glasses newImage:(NSString *)image;
+ (void)editName:(GlassesPeripheral *)glasses  newName:(NSString *)name;
+ (void)edit:(NSString *)identifier  latitude:(NSString *)lat longitude:(NSString *)lon;
+ (void)editTime:(NSString *)identifier;
+ (void)editProximity:(GlassesPeripheral *)glasses  newValue:(NSString *)value;
+ (void)editLastStatus:(GlassesPeripheral *)glasses  newValue:(NSString *)value;
@end
