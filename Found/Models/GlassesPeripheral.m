//
//  GlassesPeripheral.m
//  Glasses
//
//  Created by iBuildx-Macbook on 05/12/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "GlassesPeripheral.h"
#import <UIKit/UIKit.h>

@implementation GlassesPeripheral



- (id)initWithManagedObject:(NSManagedObject *)object{
    self = [super init];
    if(self){
        
        self.name = [object valueForKey:@"name"];
        self.image = [object valueForKey:@"image"];
        self.identifier = [object valueForKey:@"identifier"];
        self.dateInserted = [object valueForKey:@"dateInserted"];
        self.latitude = [object valueForKey:@"latitude"];
        self.longitude = [object valueForKey:@"longitude"];
        self.proximity = [object valueForKey:@"proximity"];
        self.lastStatus = [object valueForKey:@"lastStatus"];
    }
    return self;
}


+ (NSError *)createNewGlassesWithGlasses :(GlassesPeripheral *)glasses {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newGlasses = [NSEntityDescription insertNewObjectForEntityForName:@"Glasses" inManagedObjectContext:context];
    [newGlasses setValue:glasses.name forKey:@"name"];
    [newGlasses setValue:glasses.image forKey:@"image"];
    [newGlasses setValue:glasses.identifier forKey:@"identifier"];
    [newGlasses setValue:glasses.dateInserted forKey:@"dateInserted"];
    [newGlasses setValue:glasses.latitude forKey:@"latitude"];
    [newGlasses setValue:glasses.longitude forKey:@"longitude"];
    [newGlasses setValue:glasses.proximity forKey:@"proximity"];
    [newGlasses setValue:glasses.lastStatus forKey:@"lastStatus"];

    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    return error;
}

+ (NSMutableArray *)getAllGlasses {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

+ (void)deleteGlasses:(GlassesPeripheral *)glasses {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identifier == %@", glasses.identifier];
    NSArray *array = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
  
    for (NSManagedObject *managedObject in array) {
       
         [context deleteObject:managedObject];
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

+ (void)editImage:(GlassesPeripheral *)glasses newImage:(NSString *)image {
   
    [self changeValueIn:glasses withKey:@"image" forValue:image];
}

+ (void)editName:(GlassesPeripheral *)glasses  newName:(NSString *)name  {
    [self changeValueIn:glasses withKey:@"name" forValue:name];
    
}

+ (void)editProximity:(GlassesPeripheral *)glasses  newValue:(NSString *)value  {
    [self changeValueIn:glasses withKey:@"proximity" forValue:value];
    
}

+ (void)editLastStatus:(GlassesPeripheral *)glasses  newValue:(NSString *)value  {
    [self changeValueIn:glasses withKey:@"lastStatus" forValue:value];
}

+ (void)edit:(NSString *)identifier  latitude:(NSString *)lat longitude:(NSString *)lon  {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identifier == %@",identifier];
    NSArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *managedObject in array) {
        
        [managedObject setValue:lat forKey:@"latitude"];
        [managedObject setValue:lon forKey:@"longitude"];
    }
    
    NSError *error = nil; 
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        
    }
    
}

+ (void)editTime:(NSString *)identifier{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identifier == %@",identifier];
    NSArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSDate *today = [NSDate date];

    for (NSManagedObject *managedObject in array) {
        
        [managedObject setValue:today forKey:@"dateInserted"];
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        
    }
    
}




+ (void) changeValueIn:(GlassesPeripheral *)glasses withKey:(NSString *)key forValue:(NSString *)value{
    
     NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identifier == %@", glasses.identifier];
    NSArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *managedObject in array) {
        
        [managedObject setValue:value forKey:key];
    }
    
    NSError *error = nil;    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        
    }

}

+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
