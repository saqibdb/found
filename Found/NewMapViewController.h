//
//  NewMapViewController.h
//  Found
//
//  Created by iBuildX on 27/03/2018.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "GlassesPeripheral.h"

@interface NewMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UILabel *glassesNameTxt;

@property (weak, nonatomic) IBOutlet UILabel *lastSeenTxt;



@property (nonatomic) GlassesPeripheral *selectedGlasses;
@property (nonatomic, retain) CLLocationManager *locationManager;


- (IBAction)directionAction:(UIButton *)sender;

- (IBAction)actionBackButton:(UIBarButtonItem *)sender;



@end
