//
//  NewMapViewController.m
//  Found
//
//  Created by iBuildX on 27/03/2018.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

#import "NewMapViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "Found-Swift.h"

@interface NewMapViewController ()

@end

@implementation NewMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    
    self.glassesNameTxt.text = self.selectedGlasses.name;
    self.lastSeenTxt.text = [NSString stringWithFormat:@"%@",[Utils getTimeString:self.selectedGlasses.dateInserted]];
    
    CLLocationDegrees latitude = [self.selectedGlasses.latitude doubleValue];
    CLLocationDegrees longitude = [self.selectedGlasses.longitude doubleValue];
    
    //CLLocationDegrees latitude = 33.768670;
    //CLLocationDegrees longitude = 72.740406;
    
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:16.0];
    [self.mapView animateToCameraPosition:cameraPosition];
    
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];

    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = loc.coordinate;
    marker.title = self.selectedGlasses.name;
    marker.snippet = @"Hello There";
    
    
    marker.icon = [UIImage imageNamed:@"Glass Pin"];
    
    marker.map = self.mapView;
    
    self.mapView.myLocationEnabled = YES;
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"Location Updated");
}


- (void)drawRoute{
    CLLocationDegrees latitude = [self.selectedGlasses.latitude doubleValue];
    CLLocationDegrees longitude = [self.selectedGlasses.longitude doubleValue];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    
    CLLocationDegrees latitude2 = 33.769067;
    CLLocationDegrees longitude2 = 72.760299;
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:latitude2 longitude:longitude2];

    [SVProgressHUD showWithStatus:@"Getting Route..."];
    [self fetchPolylineWithOrigin:self.locationManager.location destination:loc completionHandler:^(GMSPolyline *polyline)
    //[self fetchPolylineWithOrigin:loc2 destination:loc completionHandler:^(GMSPolyline *polyline)
     {
         [SVProgressHUD dismiss];
         if(polyline)
             polyline.map = self.mapView;
     }];
}

- (void)fetchPolylineWithOrigin:(CLLocation *)origin destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    //https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyBUhBi6Hauc_7k-omB6uxBz9kDETT03MdM&sensor=true
    NSString *directionsUrlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&mode=driving&key=AIzaSyBUhBi6Hauc_7k-omB6uxBz9kDETT03MdM&sensor=true", originString, destinationString];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if(error)
        {
            if(completionHandler)
                NSLog(@"ERROR = %@" , error.description);
                completionHandler(nil);
            return;
        }
        else{
        }
        
        NSArray *routesArray = [json objectForKey:@"routes"];
        NSLog(@"GOT LINES = %lu" , (unsigned long)routesArray.count);

        GMSPolyline *polyline = nil;
        if ([routesArray count] > 0)
        {
            NSDictionary *routeDict = [routesArray objectAtIndex:0];
            NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
            NSString *points = [routeOverviewPolyline objectForKey:@"points"];
            GMSPath *path = [GMSPath pathFromEncodedPath:points];
            polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeWidth = 4;
            polyline.strokeColor = [UIColor blackColor];
        }
        
        // run completionHandler on main thread
        dispatch_sync(dispatch_get_main_queue(), ^{
            if(completionHandler)
                completionHandler(polyline);
        });
    }];
    [fetchDirectionsTask resume];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)directionAction:(UIButton *)sender {
    NSLog(@"Route Initiated");
    [self drawRoute];
}

- (IBAction)actionBackButton:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
