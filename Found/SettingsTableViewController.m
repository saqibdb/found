//
//  SettingsTableViewController.m
//  Glasses
//
//  Created by iBuildx-Macbook on 23/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "GlassesPeripheral.h"
#import "Found-Swift.h"

@interface SettingsTableViewController (){
    GlassesPeripheral *newGlasses;
    NSIndexPath *selectedIndex;
}
@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    
    newGlasses = [[GlassesPeripheral alloc] init];
    newGlasses.proximity = [Utils overallProximityString];
    
    self.switchWhenleftBehind.on = [Utils isLeftBehindDisabled];
    
    self.switchWhenBluetoothDisabled.on = [Utils notificationWhenBluetoothIsDisabled];

}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

-(void) refreshView{
    self.labelProximity.text = [Utils overallProximityString];
    self.labelDuration.text = [Utils notificationDisabledTimeString];
     
}
#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
 
    
    header.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    header.textLabel.numberOfLines = 0;
    
    
    
    
    
    
    NSString *text = [NSString stringWithFormat:@"NOTIFICATIONS\nThis lets you set how far you will be from your glasses to be alerted."];
    
    if (section == 0) {
        text = [NSString stringWithFormat:@"NOTIFICATIONS\nThis lets you set on what you should be notified by the app."];
        
    }
    else if (section == 1){
        text = [NSString stringWithFormat:@"SETTING PROXIMITY\nThis lets you set how far you will be from your glasses to be alerted."];
    }
    else if (section == 2){
        text = [NSString stringWithFormat:@"DISABLE NOTIFICATIONS\nThis lets you set after how much time a notification is repeated."];
    }
    else if (section == 3){
        text = [NSString stringWithFormat:@"ABOUT\nThis is information about our company."];
    }
    
    // If attributed text is supported (iOS6+)
    if ([header.textLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: header.textLabel.textColor,
                                  NSFontAttributeName: header.textLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        header.textLabel.attributedText = attributedText;
    }
    // If attributed text is NOT supported (iOS5-)
    else {
        header.textLabel.text = text;
    }
    
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60.0;
}


- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.textAlignment = NSTextAlignmentCenter;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex = indexPath;
    if (indexPath.section == 1 && indexPath.row == 0) {
        
        [self performSegueWithIdentifier:@"ToProximity" sender:self];
    }
    if (indexPath.section == 2 && indexPath.row == 0) {
        [self performSegueWithIdentifier:@"ToProximity" sender:self];
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ToProximity"]) {
        ProximitySettingsViewController *vc = segue.destinationViewController;
        vc.isSettingsPage = YES;
         if (selectedIndex.section == 1 && selectedIndex.row == 0) {
             vc.isProximity = YES;
             vc.title = @"Proximity";
         }else{
             vc.title = @"Disable  Notifications";
         }
    }
}

- (IBAction)actionLeftBehind:(id)sender {
    
    [self setLeftBehind:self.switchWhenleftBehind.isOn];
  
}
- (IBAction)actionWhenBluetoothisDisabled:(id)sender {
    
    [self setWhenBluetoothisDisabled:self.switchWhenBluetoothDisabled.isOn];

}

@end
