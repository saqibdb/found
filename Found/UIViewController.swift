//
//  UIViewController.swift
//  Glasses
//
//  Created by Jefri Singh on 07/03/18.
//  Copyright © 2018 Hamza Temp. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@objc extension UIViewController{
    
    func isBluetoothOn()->Bool{
        
        #if swift(<5.1)
        print("Less than 5.1")
        #else
        print("At least 5.1")
        #endif
        
        let central = LGCentralManager.sharedInstance()
        return central?.isCentralReady == true
        
    }
    
    func checkBluetooth(isRepeat:Bool) {
        
        let central = LGCentralManager.sharedInstance()
        
        if central?.isCentralReady == false && central?.centralNotReadyReason != "The platform/hardware doesn't support Bluetooth Low Energy."{
            
            
            
            
            if Utils.notificationWhenBluetoothIsDisabled() == true {
                
                if central?.centralNotReadyReason != "Central not initialized yet." {
                    self.showPermissionAlert(central?.centralNotReadyReason as String? ?? "", message:  nil, buttonOkay: "OK", buttonCancel: "Cancel", completion: { (bool) in
                        
                        if bool{
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(URL.init(string:"prefs:root=Settings")! , options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                            } else {
                                UIApplication.shared.canOpenURL(URL.init(string: "prefs:root=Settings")!)
                            }
                        }else{
                            if isRepeat{
                                //self.checkBluetooth(isRepeat: isRepeat)
                            }
                        }
                    })
                }
                
                
                
            }
        }else{
            self.alert(central?.centralNotReadyReason as String? ?? "")
        }
        
    }
    
    func getTime(_ from:NSDate)->String{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        return formatter.string(from: from as Date)
    }
    
    func alert(_ message:String){
        
        if message.isEmpty == false {
            let controller = UIAlertController.init(title: message, message: nil, preferredStyle: .alert)
            let actionCancel = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
            controller.addAction(actionCancel)
            self.present(controller, animated: true, completion: nil)
        }
        
        
    }
    
    func showPermissionAlert(_ title:String?, message:String?,buttonOkay:String,buttonCancel:String?, completion: @escaping (_ action:Bool)->Void){
        
        let controller = UIAlertController(title: title == nil ? "Alert" : title!, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonCancel == nil ? "Cancel" : buttonCancel!, style: .cancel, handler:  ({(action) in
            completion(false)
            
        }))
        let settingsAction = UIAlertAction(title: buttonOkay, style: .default, handler: ({(action) in
            completion(true)
            
        }))
        
        controller.addAction(settingsAction)
        controller.addAction(action)
        self.present(controller, animated: true, completion: nil)
    }
    
    func setLeftBehind(_ bool:Bool){
        
        UserDefaults.standard.set(bool, forKey: "isLeftBehind")
    }
    
    func setWhenBluetoothisDisabled(_ bool:Bool){
        
        UserDefaults.standard.set(bool, forKey: "isBluetoothDisabledThenWantNotification")
    }
    
    func setDisableNotification(_ bool:Bool){
        
        UserDefaults.standard.set(bool, forKey: "isNotificationDisabled")
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
