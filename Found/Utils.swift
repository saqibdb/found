//
//  Utils.swift
//  MapRoutingHERE
//
//  Created by Sreelash Sasikumar on 11/5/16.
//  Copyright © 2016 Sreelash Sasikumar. All rights reserved.
//

import UIKit
import UserNotifications

@objc
public class Utils: NSObject {
    
    @objc
    class func getTimeString(_ date:Date)->String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm at"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        let stringValue = "Found at \(formatter.string(from: date))"
        
        //print("String values is\(stringValue)")
        return stringValue
    
    }
    @objc
    class func isNotificationDisabled()->Bool{
        
       return  UserDefaults.standard.bool(forKey: "isNotificationDisabled")
    }
    
    @objc
    class func notificationWhenBluetoothIsDisabled()->Bool{
        
        return UserDefaults.standard.bool(forKey: "isBluetoothDisabledThenWantNotification")
    }
    
    @objc
    class func isLeftBehindDisabled()->Bool{
        
        return  UserDefaults.standard.bool(forKey: "isLeftBehind")
    }
    
    @objc
    class func notificationDisableTime()->Int{
        
        return UserDefaults.standard.integer(forKey: "notificationDisableTime")
    }
    
    @objc
    class func overallProximity()->Int{
        
        return UserDefaults.standard.integer(forKey: "proximity")
    }
    
    @objc
    class func overallProximityString()->String{
        
        return overallProximity() == 0 ? "" : String(overallProximity())+" ft"
    }
    
    @objc
    class func notificationDisabledTimeString()->String{
        
        return notificationDisableTime() == 0 ? "" : String(notificationDisableTime())+(notificationDisableTime() == 1 ? " ft" : " fts")
    }
    
    @objc
    class func showLocalNotofication(title:String,body:String){

        
        let applicationDelegate = UIApplication.shared.delegate as! AppDelegate
        UNUserNotificationCenter.current().delegate = applicationDelegate
     
        let content = UNMutableNotificationContent()
        content.body = body
        content.title = title
        content.sound = UNNotificationSound.default
        
        let request = UNNotificationRequest(identifier: "fred", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request)
        {
            error in // called when message has been sent
            
            debugPrint("Error: \(error)")
        }
    }
    
    @objc
    func addCategory() {
        // Add action.
        let stopAction = UNNotificationAction(identifier: "stop", title: "Stop", options: [])
        let snoozeAction = UNNotificationAction(identifier: "workout", title: "Workout", options: [])
        
        // Create category.
        let category = UNNotificationCategory(identifier: "workout", actions: [stopAction, snoozeAction], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
    }
}
