//
//  ViewController.m
//  Glasses
//
//  Created by Hamza Temp on 12/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "ViewController.h"
#import "MyGlassesTableViewCell.h"
#import <LGBluetooth.h>
#import "GlassesPeripheral.h"
#import "MapViewController.h"
#import "AddNewDeviceTableViewCell.h"
#import "Found-Swift.h"
#import "ProximitySetting.h"
#import "NewGlassesTableViewCell.h"




@interface ViewController (){
    NSMutableArray *allFoundPerphs;
    GlassesPeripheral *selectedGlasses;
    UIImage *choosenImage;
    NSString *latitude;
    NSString *longitude;
    NSInteger selectedIndex;
}

@end

@implementation ViewController 
@synthesize tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];

    [LGCentralManager sharedInstance];

    self.glassesTableView.delegate = self;
    self.glassesTableView.dataSource = self;

    
    self.nGlassesTableView.tag = 2;
    self.nGlassesTableView.delegate = self;
    self.nGlassesTableView.dataSource = self;

    
    self.nGlassesTableView.hidden = YES;
    
   
    [self createNavigationTitleViewWithTitle:@""];

    //[self createNewGlasses];

    
    self.glassesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"com.location.event" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventHandler:) name:@"com.background.event" object:nil ];
}

//event handler when event occurs
-(void)eventHandler: (NSNotification *) notification
{
    if(self.isBluetoothOn == YES){
        _myGlassesArray = [GlassesPeripheral getAllGlasses];
        if (_myGlassesArray && _myGlassesArray.count) {
            self.glassesTableView.hidden = NO;
            [self.glassesTableView reloadData];
        }
        [self scanWithTime:2];
    }else{
        [self checkBluetoothWithIsRepeat:YES];
    }
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSDictionary *dict = notification.userInfo;
    latitude = [dict valueForKey:@"latitude"];
    longitude = [dict valueForKey:@"longitude"];
}

-(void)viewDidAppear:(BOOL)animated {
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     if(self.isBluetoothOn == YES){
          [self reloadTableView];
          [self scanWithTime:2];
      }else{
          [self checkBluetoothWithIsRepeat:YES];
      }
      [self reloadTableView];
    });
    
    
    
}

-(void) reloadTableView{
    
    _myGlassesArray = [GlassesPeripheral getAllGlasses];
    if (_myGlassesArray && _myGlassesArray.count) {
        self.glassesTableView.hidden = NO;
        [self.glassesTableView reloadData];
    }else{
        self.glassesTableView.hidden = YES;
    }
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag == 2){
        return allFoundPerphs.count;
    }
    else{
        if (_myGlassesArray.count) {
            return _myGlassesArray.count + 1;
        }
        else{
            return 0;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 2){
        NewGlassesTableViewCell *cell = (NewGlassesTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"NewGlassesTableViewCell" forIndexPath:indexPath];

        
        LGPeripheral *perph = allFoundPerphs[indexPath.row];
        if(perph.name != nil){
            cell.nameTitle.text = perph.name;
        }
        else if ([perph.advertisingData objectForKey:@"kCBAdvDataLocalName"] != nil){
            cell.nameTitle.text = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
        }
        else{
            cell.nameTitle.text = @"Unamed";
        }
        cell.subtitle.text = [NSString stringWithFormat:@"%lu services found" , (unsigned long)perph.services.count];

        
        
        return cell;
    }
    else{
        if (indexPath.row < _myGlassesArray.count) {
                MyGlassesTableViewCell *cell = (MyGlassesTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"MyGlassesCell" forIndexPath:indexPath];
                NSManagedObject *glasees = [_myGlassesArray objectAtIndex:indexPath.row];
                cell.name.text = [glasees valueForKey:@"name"];
                cell.time.text = [Utils getTimeString:[glasees valueForKey:@"dateInserted"]];
                cell.statusImage.image = [UIImage imageNamed:@"OvalOff"];
                cell.goingNextBtn.tag = indexPath.row;
        //        cell.time.text = [self getTime:[glasees valueForKey:@"dateInserted"]];
                [cell.goingNextBtn addTarget:self action:@selector(goNextPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                //NSLog(@"Latitude is %@ and longitude is %@",latitude,longitude);
                
                //NSLog(@"Date string is %@",[Utils getTimeString:[glasees valueForKey:@"dateInserted"]]);
                
                BOOL foundInCurrentDevices = NO;
                
                for (LGPeripheral *perph in allFoundPerphs) {
                    
                    if ([perph.UUIDString isEqualToString:[glasees valueForKey:@"identifier"]]) {
                        
                        cell.name.text = [glasees valueForKey:@"name"];//[perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
                        cell.statusImage.image = [UIImage imageNamed:@"OvalOn"];
                        [GlassesPeripheral edit:perph.UUIDString latitude:latitude longitude:longitude];
                        [GlassesPeripheral editTime:perph.UUIDString];
                        cell.time.text = [Utils getTimeString:[[NSDate alloc] init] ];
                        
                        foundInCurrentDevices = YES;
                        break;
                    }
                }
                
                
                if (foundInCurrentDevices == YES) {
                    [GlassesPeripheral editLastStatus:[_myGlassesArray objectAtIndex:indexPath.row] newValue:@"online"];
                }
                else{
                    NSString *lastOnlineStr = [glasees valueForKey:@"lastStatus"];
                    if ([lastOnlineStr isEqualToString:@"online"]) {
                        
                        
                        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                        
                        BOOL isLeftBehind = [preferences boolForKey:@"isLeftBehind"];
                        
                        
                        if (isLeftBehind == YES){
                            
                            NSString *msg = [NSString stringWithFormat:@"You left your glasses behind!"];
                            
                            if ([glasees valueForKey:@"name"] != nil) {
                                msg = [NSString stringWithFormat:@"You left your %@ glasses behind!", [glasees valueForKey:@"name"]];
                            }
                            
                            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Out of Proximity" message:msg preferredStyle:UIAlertControllerStyleAlert];
                            
                            [actionSheet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
                            [self presentViewController:actionSheet animated:YES completion:nil];
                        }
                        else{
                        }
                    }
                    
                    [GlassesPeripheral editLastStatus:[_myGlassesArray objectAtIndex:indexPath.row] newValue:@"offline"];

                }
         
                cell.glassesImage.image = [self decodeBase64ToImage:[glasees valueForKey:@"image"]];
                
                cell.ellipsisBtn.tag = indexPath.row;
                [cell.ellipsisBtn addTarget:self action:@selector(editBtnPressed:) forControlEvents:UIControlEventTouchUpInside];

                return cell;
            }
            else{
                AddNewDeviceTableViewCell *cell = (AddNewDeviceTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"AddNewDeviceTableViewCell" forIndexPath:indexPath];
                return cell;
            }
    }

}

- (IBAction)actionScan:(UIButton *)sender {
    
    if(self.isBluetoothOn == YES){
        [self performSegueWithIdentifier:@"ToAddNew" sender:self];
    }else{

        [self checkBluetoothWithIsRepeat:YES];
    }

}



- (IBAction)goNextPressed:(UIButton *)sender {
    selectedGlasses = _myGlassesArray[sender.tag];
    [self performSegueWithIdentifier:@"mapIdObj" sender:self];
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    //NSInteger selectedRow = indexPath.row; //this is the number row that was selected
    if (indexPath.row < _myGlassesArray.count) {
        selectedGlasses = _myGlassesArray[indexPath.row];
   
        selectedIndex = indexPath.row+1;
        
        
        UIButton *btn = [[UIButton alloc] init];
        
        btn.tag = indexPath.row;
        
        [self goNextPressed:btn];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        

    }
    else{
        //ToAddNew
   
        if(self.isBluetoothOn == YES){

           [self performSegueWithIdentifier:@"ToAddNew" sender:tableView];
        }else{

            [self checkBluetoothWithIsRepeat:YES];
        }
    
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 2){
        return 65;
    }
    else{
        if (indexPath.row < _myGlassesArray.count) {
            return 110.0;
        }
        else{
            return 40;
        }
    }
}



-(void)scanWithTime :(int)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        
        if(self.isBluetoothOn == NO){
            
            [self checkBluetoothWithIsRepeat:YES]; 
            return;
        }
        if (![[LGCentralManager sharedInstance] isCentralReady]) {
            NSLog(@"Reason is %@" ,[[LGCentralManager sharedInstance] centralNotReadyReason] );
            return;
        }
        
        
        // Scaning 4 seconds for peripherals
        [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:time
                                                             completion:^(NSArray *peripherals)
         {
             // If we found any peripherals sending to test
             if (peripherals.count) {
                 
                 
                 NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                 
                 NSString *currentLevelKey = @"FoundProximity";
                 
                 if ([preferences objectForKey:currentLevelKey] == nil){
                     //  Doesn't exist.
                     allFoundPerphs = [[NSMutableArray alloc] initWithArray:peripherals];
                     [self.nGlassesTableView reloadData];
                 }
                 else{
                     //  Get current level
                     allFoundPerphs = [[NSMutableArray alloc] init];

                     const NSInteger currentLevel = [preferences integerForKey:currentLevelKey];
                     
                     for (LGPeripheral *perph in peripherals) {//75
                         
                         int proxityNow = (int)floor([self estimatedDistanceWithRSSI:@(perph.RSSI) calibrationPower:@(-75)]);
                         
                         NSLog(@"RSSI  = %ld name = %@ Distance = %i" , (long)perph.RSSI , [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"], proxityNow);

                         
                         ;

                         if (currentLevel > proxityNow) {
                             [allFoundPerphs addObject:perph];
                         }
                         else{
                             NSLog(@"Out of proximity");
                         }
                     }
                     
                 }
                 
                 //[self testPeripheral:peripherals[0]];
                 //[self putPeripheralOnFound:perph];
                 

                 [self scanWithTime: 2];
                 
                 [self.glassesTableView reloadData];
                 [self.nGlassesTableView reloadData];

             }
             else{
                 NSLog(@"NONE");
                 [self scanWithTime: 2];
                 allFoundPerphs = [[NSMutableArray alloc] init];
                 [self.glassesTableView reloadData];
                 [self.nGlassesTableView reloadData];

             }
         }];
        
    });
    
}

-(double)calculateNewDistanceWithTxCalibratedPower :(int)txCalibratedPower andRSSI :(int)rssi {
    if(rssi == 0){
        return -1;
    }
    double ratio = (rssi * 1.0)/txCalibratedPower;
    
    if (ratio < 1.0) {
        return pow(10.0, ratio);
    }
    else{
        double accuracy = 0.89976 * pow(ratio, 7.7095) + 0.111;
        return accuracy;
    }
}
-(double)realDistanceFromBeaconRssi:(NSInteger)rssi defaultRssi:(NSInteger)defaultRssi {
    if (rssi == 0) {
        return -1;
    }
    
    double ratio = fabs((double)rssi / (double)defaultRssi);
    if (ratio < 1) {
        return pow(ratio, 10);
    }
    
    return 0.89976 * pow(ratio, 7.7095) + 0.111;
}

-(float)estimatedDistanceWithRSSI:(NSNumber *)RSSI calibrationPower:(NSNumber *)calibrationPower {
    if (calibrationPower == 0) {
        return -1.0; // if we cannot determine distance, return -1.
    }
    
    float rssi = [RSSI floatValue];
    if (rssi == 0) {
        return -1.0; // if we cannot determine distance, return -1.
    }
    
    double ratio = rssi*1.0/[calibrationPower floatValue];
    if (ratio < 1.0) {
        return (pow(ratio,10) * 3.28084);
    } else {
        double accuracy =  (0.89976)*pow(ratio,7.7095) + 0.111;
        return (accuracy * 3.28084);
    }
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"mapIdObj"]) {
         Map *vc = segue.destinationViewController;
         vc.selectedGlasses = selectedGlasses;
     }else if ([segue.identifier isEqualToString:@"ToProximity"]) {
          ProximitySettingsViewController *vc = segue.destinationViewController;
         vc.selectedGlasses = selectedGlasses;
     }
 }


- (IBAction)editBtnPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    selectedIndex = btn.tag + 1;
    selectedGlasses = _myGlassesArray[btn.tag];

    if(selectedIndex > 0){
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Edit" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: selectedGlasses.name message: @"Enter the name of the BLE device!" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"name";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.text = selectedGlasses.name;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            [GlassesPeripheral editName:selectedGlasses newName:namefield.text];
            [self.glassesTableView reloadData];
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [GlassesPeripheral deleteGlasses:selectedGlasses];
        selectedIndex = 0;
        [self reloadTableView];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Set Proximity" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self performSegueWithIdentifier:@"ToProximity" sender:self];

    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Glasses Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        
    }]];
    
    
    //ToProximity
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
        selectedIndex = 0;
    }else{
        [self alert:@"Please select the glass to edit!"];
    }
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    choosenImage = chosenImage;
    
    NSString *encoded = [self encodeToBase64String:choosenImage];
    [GlassesPeripheral editImage:selectedGlasses newImage:encoded];
    [self reloadTableView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"foundLogoSmall"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(0, -2, 40, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
  
    
    CGRect applicationFrame = CGRectMake(0, 0, 40, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    
    self.navigationItem.titleView = newView;
}



- (IBAction)actionBluetooth:(id)sender {
}

//MARK:- Custom functions

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}
@end
